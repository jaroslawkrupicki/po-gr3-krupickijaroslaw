package pl.imiajd.krupicki;

import java.awt.*;

public class BetterRectangle extends Rectangle {

    public BetterRectangle(int w, int h){
        //setLocation(0, 0);
        //setSize(w, h);
        super(w, h);
    }

    public double getPerimeter(){
        return (super.getWidth() + super.getHeight()) * 2;
    }

    public double getArea(){
        return super.getWidth() * super.getHeight();
    }
}
