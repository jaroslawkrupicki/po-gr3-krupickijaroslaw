package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium04;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Zadanie_5 {

    public static void main(String[] args) {
        int k = 1000;
        double p = 0.02;
        int n = 4;
        System.out.println(ile(k, p, n));
    }

    public static String ile(int k, double p, int n){
        BigDecimal bd = new BigDecimal(String.valueOf(k));
        for(int i = 0; i < n; i++){
            bd = bd.add(BigDecimal.valueOf(bd.doubleValue() * p));
        }
        return bd.setScale(2, RoundingMode.HALF_UP).toString();
    }
}
