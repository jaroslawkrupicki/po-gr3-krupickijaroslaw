package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium04;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zadanie_3 {

    public static void main(String[] args) {
        String file = "src/Zadanie_3.txt";
        String str = "wyraz";
        System.out.println(countChar(file, str));
    }

    public static int countChar(String file, String str){
        int i = 0;
        try {
            Scanner scanner = new Scanner(new File(file));
            while(scanner.hasNextLine()){
                String line = scanner.nextLine();
                i += line.split(String.valueOf(str), -1).length - 1;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return i;
    }
}
