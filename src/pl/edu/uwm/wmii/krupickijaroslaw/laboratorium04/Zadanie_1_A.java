package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium04;

public class Zadanie_1_A {

    public static void main(String[] args) {
        String str = "Aparat";
        char c = 'a';
        System.out.println(countChar(str, c));
    }

    public static int countChar(String str, char c){
        return str.split(String.valueOf(c), -1).length - 1;
    }
}
