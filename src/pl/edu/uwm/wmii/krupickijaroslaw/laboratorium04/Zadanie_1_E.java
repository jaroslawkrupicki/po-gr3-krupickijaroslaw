package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium04;

public class Zadanie_1_E {

    public static void main(String[] args) {
        String str = "Aparat aparat";
        String subStr = "ar";
        for(int i : where(str, subStr)){
            System.out.println(i);
        }
    }

    public static int[] where(String str, String subStr){
        int[] tab = new int[str.split(subStr, -1).length - 1];
        int nextTabIndex = 0;
        int lastIndex = 0;
        while(lastIndex != -1){
            lastIndex = str.indexOf(subStr, lastIndex);
            if(lastIndex != -1) {
                tab[nextTabIndex] = lastIndex;
                lastIndex += subStr.length();
                nextTabIndex++;
            }
        }
        return tab;
    }
}
