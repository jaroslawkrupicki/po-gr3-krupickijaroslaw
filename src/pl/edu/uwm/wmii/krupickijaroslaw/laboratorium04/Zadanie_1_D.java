package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium04;

public class Zadanie_1_D {

    public static void main(String[] args) {
        String str = "Aparat";
        int n = 3;
        System.out.println(repeat(str, n));
    }

    public static String repeat(String str, int n){
        StringBuilder s = new StringBuilder();
        for(int i = 0; i < n; i++){
            s.append(str);
        }
        return s.toString();
    }
}
