package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium04;

public class Zadanie_1_H {

    public static void main(String[] args) {
        String str = "5843045832";
        String s = "-";
        int n = 4;
        System.out.println(nice(str, s, n));
    }

    public static String nice(String str, String s, int n){
        StringBuffer sb = new StringBuffer();
        char[] c = str.toCharArray();
        int j = 1;
        for(int i = c.length - 1; i >= 0; i--){
            sb.append(c[i]);
            if(j % n == 0){
                j = 1;
                sb.append(s);
            }
            else j++;
        }
        return sb.reverse().toString();
    }
}
