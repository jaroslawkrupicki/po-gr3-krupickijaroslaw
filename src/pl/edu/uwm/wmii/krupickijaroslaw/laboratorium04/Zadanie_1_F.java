package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium04;

public class Zadanie_1_F {

    public static void main(String[] args) {
        String str = "Aparat";
        System.out.println(change(str));
    }

    public static String change(String str){
        char[] c = str.toCharArray();
        for(int i = 0; i < str.length(); i++){
            if(Character.isLowerCase(c[i])) c[i] = Character.toUpperCase(c[i]);
            else if(Character.isUpperCase(c[i])) c[i] = Character.toLowerCase(c[i]);
        }
        StringBuffer sb = new StringBuffer();
        sb.append(c);
        return sb.toString();
    }
}
