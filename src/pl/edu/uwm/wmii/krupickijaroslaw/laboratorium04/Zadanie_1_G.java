package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium04;

public class Zadanie_1_G {

    public static void main(String[] args) {
        String str = "5843045832";
        System.out.println(nice(str));
    }

    public static String nice(String str){
        StringBuffer sb = new StringBuffer();
        char[] c = str.toCharArray();
        int j = 1;
        for(int i = c.length - 1; i >= 0; i--){
            sb.append(c[i]);
            if(j % 3 == 0){
                j = 1;
                sb.append("'");
            }
            else j++;
        }
        return sb.reverse().toString();
    }
}
