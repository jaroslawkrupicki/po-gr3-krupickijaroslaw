package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium04;

import java.math.BigInteger;

public class Zadanie_4 {

    public static void main(String[] args) {
        int n = 4;
        System.out.println(ile(n));
    }

    public static String ile(int n){
        BigInteger bi = new BigInteger("0");
        for(int i = 0; i < n * n; i++){
            bi = bi.add(BigInteger.valueOf((long)Math.pow(2, i)));
        }
        return bi.toString();
    }
}
