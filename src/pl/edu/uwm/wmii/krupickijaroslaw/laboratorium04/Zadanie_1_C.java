package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium04;

public class Zadanie_1_C {

    public static void main(String[] args) {
        String str = "Aparat";
        System.out.println(middle(str));
    }

    public static String middle(String str){
        if(str.length() % 2 == 0){
            return String.valueOf(str.charAt(str.length() / 2 - 1)) + String.valueOf(str.charAt(str.length() / 2));
        }
        else{
            return String.valueOf(str.charAt(str.length() / 2));
        }
    }
}
