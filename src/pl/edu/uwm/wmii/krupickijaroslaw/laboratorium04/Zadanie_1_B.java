package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium04;

public class Zadanie_1_B {

    public static void main(String[] args) {
        String str = "Aparat aparat";
        String subStr = "rat";
        System.out.println(countSubStr(str, subStr));
    }

    public static int countSubStr(String str, String subStr){
        return str.split(subStr, -1).length - 1;
    }
}
