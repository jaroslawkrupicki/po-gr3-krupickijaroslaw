package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium14lambda;

public interface Measurable{

	double getMeasure();

	static double average(Measurable[] objects){
		double avg = 0;
		for(Measurable measurable : objects) avg += measurable.getMeasure();
		return avg / objects.length;
	}

	static Measurable largest(Measurable[] objects){
		if(objects == null || objects.length == 0) return null;
		Measurable m = objects[0];
		for(Measurable measurable : objects){
			if(measurable.getMeasure() > m.getMeasure()){
				m = measurable;
			}
		}
		return m;
	}
}
