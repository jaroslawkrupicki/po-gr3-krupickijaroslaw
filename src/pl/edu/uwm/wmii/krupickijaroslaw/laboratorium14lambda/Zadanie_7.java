package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium14lambda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Zadanie_7{

	public static void main(String[] args){
		ArrayList<String> strings = new ArrayList<>();
		strings.add("z");
		strings.add("d");
		strings.add("g");
		strings.add("ee");
		strings.add("a");
		strings.add("ccc");
		strings.add("cc");
		strings.add("iii");
		strings.add("d");
		luckySort(strings, (o1, o2) -> {
			if(o1.length() < o2.length()) return -1;
			if(o1.length() == o2.length()) return 0;
			return 1;
		});
		System.out.println(strings);
	}

	public static void luckySort(ArrayList<String> strings, Comparator<String> comp){
		int i = 1;
		while(i < strings.size()){
			if(comp.compare(strings.get(i - 1), strings.get(i)) > 0){
				Collections.shuffle(strings);
				i = 1;
				continue;
			}
			i++;
		}
	}
}
