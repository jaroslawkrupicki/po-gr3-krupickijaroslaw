package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium14lambda;

public class Zadanie_9{

	public static void main(String[] args){
		Greeter g1 = new Greeter(1000, "AAA");
		Greeter g2 = new Greeter(1000, "BBB");
		runTogether(g1, g2);
		runInOrder(g1, g2);
	}

	public static void runTogether(Runnable... tasks){
		for(Runnable task : tasks) new Thread(task).start();
	}

	public static void runInOrder(Runnable... tasks){
		for(Runnable task : tasks) task.run();
	}

	public static class Greeter implements Runnable{

		private int n;
		private String target;

		public Greeter(int n, String target){
			this.n = n;
			this.target = target;
		}

		@Override
		public void run(){
			int i = n;
			while(i > 0){
				System.out.println("Witaj, " + target);
				i--;
			}
		}
	}
}
