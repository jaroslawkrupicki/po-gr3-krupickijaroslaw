package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium14lambda;

public class Zadanie_8{

	public static void main(String[] args){
		Greeter g1 = new Greeter(100, "AAA");
		Greeter g2 = new Greeter(100, "BBB");
		Thread t1 = new Thread(g1);
		t1.start();
		Thread t2 = new Thread(g2);
		t2.start();
	}

	public static class Greeter implements Runnable{

		private int n;
		private String target;

		public Greeter(int n, String target){
			this.n = n;
			this.target = target;
		}

		@Override
		public void run(){
			int i = n;
			while(i > 0){
				System.out.println("Witaj, " + target);
				i--;
			}
		}
	}
}
