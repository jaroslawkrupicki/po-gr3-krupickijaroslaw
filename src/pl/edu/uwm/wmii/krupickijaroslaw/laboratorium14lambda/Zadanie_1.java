package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium14lambda;

public class Zadanie_1{

	public static void main(String[] args){
		Employee[] employees = new Employee[4];
		employees[0] = new Employee("A", 5);
		employees[1] = new Employee("B", 8);
		employees[2] = new Employee("C", 2);
		employees[3] = new Employee("D", 4);
		System.out.println(Measurable.average(employees));
		Measurable measurable = Measurable.largest(employees);
		System.out.println(((Employee)measurable).getName());
	}
}
