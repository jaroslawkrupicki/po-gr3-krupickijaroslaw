package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium14lambda;

public class Employee implements Measurable{

	private String name;
	private double measure;

	public Employee(String name, double measure){
		this.name = name;
		this.measure = measure;
	}

	@Override
	public double getMeasure(){
		return measure;
	}

	public String getName(){
		return name;
	}
}
