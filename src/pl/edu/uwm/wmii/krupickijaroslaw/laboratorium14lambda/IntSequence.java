package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium14lambda;

public class IntSequence{

	public static String of(int... args){
		StringBuilder s = new StringBuilder();
		for(int arg : args) s.append(arg);
		return s.toString();
	}

	public static String constant(int i){
		while(true) System.out.println(i);
	}
}
