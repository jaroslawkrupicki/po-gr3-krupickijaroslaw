package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium14lambda;

import java.io.File;

public class Zadanie_11{

	public static void main(String[] args){
		File file = new File("src/");
		for(String f : getAllFiles(file, "txt")){
			System.out.println(f);
		}
	}

	public static String[] getAllFiles(File dir, String type){
		return dir.list((dir1, name) -> name.endsWith("." + type));
	}
}
