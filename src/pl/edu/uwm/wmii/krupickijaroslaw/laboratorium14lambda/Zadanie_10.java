package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium14lambda;

import java.io.File;

public class Zadanie_10{

	public static void main(String[] args){
		File file = new File("src/pl/edu/uwm/wmii/krupickijaroslaw");
		for(File f : getAllSubDirectories(file)){
			System.out.println(f.getName());
		}
	}

	public static File[] getAllSubDirectories(File file){
		return file.listFiles(File::isDirectory);
	}
}
