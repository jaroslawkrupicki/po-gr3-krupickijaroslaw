package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium13;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Zadanie_3{

	private static TreeMap<Student, String> studenci = new TreeMap<>();

	public static void dodaj(String imie, String nazwisko, String ocena){
		String a1 = imie.substring(0, 1).toUpperCase();
		String b1 = imie.substring(1).toLowerCase();
		imie = a1 + b1;
		String a2 = nazwisko.substring(0, 1).toUpperCase();
		String b2 = nazwisko.substring(1).toLowerCase();
		nazwisko = a2 + b2;
		Student student = new Student(imie, nazwisko);
		studenci.put(student, ocena.toLowerCase());
	}

	public static void usun(int id){
		for(Map.Entry<Student, String> map : studenci.entrySet()){
			Student student = map.getKey();
			if(student.getId() == id){
				studenci.remove(student);
				return;
			}
		}
		System.out.println("Student o podanym id nie istnieje.");
	}

	public static void zmienOcene(int id, String ocena){
		for(Map.Entry<Student, String> map : studenci.entrySet()){
			Student student = map.getKey();
			if(student.getId() == id){
				studenci.put(student, ocena.toLowerCase());
				return;
			}
		}
		System.out.println("Student o podanym id nie istnieje.");
	}

	public static void wypisz(){
		System.out.println("#### LISTA ####");
		for(Map.Entry<Student, String> map : studenci.entrySet()){
			Student student = map.getKey();
			String ocena = map.getValue();
			System.out.println("(" + student.getId() + ") " + student.getImie() + " " + student.getNazwisko() + ": " + ocena);
		}
		System.out.println("###############");
	}

	public static void main(String[] args){
		while(true){
			String input = "";
			Scanner scanner = new Scanner(System.in);
			if(scanner.hasNextLine()){
				input = scanner.nextLine();
			}
			String[] input_splitted = input.split(" ");
			if(input_splitted[0].equalsIgnoreCase("dodaj")){
				if(input_splitted.length >= 4){
					dodaj(input_splitted[1], input_splitted[2], input_splitted[3]);
				}
			}
			else if(input_splitted[0].equalsIgnoreCase("usun")){
				if(input_splitted.length >= 2){
					usun(Integer.parseInt(input_splitted[1]));
				}
			}
			else if(input_splitted[0].equalsIgnoreCase("zmien")){
				if(input_splitted.length >= 3){
					zmienOcene(Integer.parseInt(input_splitted[1]), input_splitted[2]);
				}
			}
			else if(input_splitted[0].equalsIgnoreCase("zakoncz")){
				return;
			}
			wypisz();
		}
	}

	public static class Student implements Comparable<Student>{

		private static int globalId = 0;

		private int id;
		private String imie;
		private String nazwisko;

		public Student(String imie, String nazwisko){
			globalId++;
			this.id = globalId;
			this.imie = imie;
			this.nazwisko = nazwisko;
		}

		public String getImie(){
			return imie;
		}

		public String getNazwisko(){
			return nazwisko;
		}

		public int getId(){
			return id;
		}

		@Override
		public int compareTo(Student o){
			if(nazwisko.equalsIgnoreCase(o.getNazwisko())){
				if(imie.equalsIgnoreCase(o.getImie())){
					if(id < o.getId()) return -1;
					if(id > o.getId()) return 1;
					return 0;
				}
				return imie.compareTo(o.getImie());
			}
			return nazwisko.compareTo(o.getNazwisko());
		}
	}
}
