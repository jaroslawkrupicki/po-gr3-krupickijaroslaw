package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium13;

import java.util.PriorityQueue;
import java.util.Scanner;

public class Zadanie_1{

	private static PriorityQueue<ToDoTask> toDoTasks = new PriorityQueue<>();

	public static void main(String[] args){
		while(true){
			String input = "";
			Scanner scanner = new Scanner(System.in);
			if(scanner.hasNextLine()){
				input = scanner.nextLine();
			}
			String[] input_splitted = input.split(" ");
			if(input_splitted[0].equalsIgnoreCase("dodaj")){
				if(input_splitted.length >= 3){
					int priority = Integer.parseInt(input_splitted[1]);
					if(priority < 1){
						System.out.println("Priorytet musi byc liczba dodatnia.");
						continue;
					}
					String description = "";
					for(int i = 2; i < input_splitted.length; i++){
						if(i == 2) description += input_splitted[i];
						else description += " " + input_splitted[i];
					}
					ToDoTask toDoTask = new ToDoTask(priority, description);
					toDoTasks.add(toDoTask);
				}
			}
			else if(input_splitted[0].equalsIgnoreCase("nastepne")){
				toDoTasks.poll();
			}
			else if(input_splitted[0].equalsIgnoreCase("zakoncz")){
				return;
			}
			showToDoTasks();
		}
	}

	public static void showToDoTasks(){
		System.out.println("#### LISTA ZADAN ####");
		for(ToDoTask task : toDoTasks){
			System.out.println(task.getPriority() + " | " + task.getDescription());
		}
		System.out.println("#####################");
	}

	public static class ToDoTask implements Comparable<ToDoTask>{

		private int priority;
		private String description;

		public ToDoTask(int priority, String description){
			this.priority = priority;
			this.description = description;
		}

		public int getPriority(){
			return priority;
		}

		public void setPriority(int priority){
			this.priority = priority;
		}

		public String getDescription(){
			return description;
		}

		public void setDescription(String description){
			this.description = description;
		}

		@Override
		public int compareTo(ToDoTask task){
			if(priority == task.getPriority()) return 0;
			if(priority < task.getPriority()) return -1;
			return 1;
		}
	}
}
