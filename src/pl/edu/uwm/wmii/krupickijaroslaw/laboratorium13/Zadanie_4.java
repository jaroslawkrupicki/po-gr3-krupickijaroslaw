package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium13;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;

public class Zadanie_4{

	private static HashMap<Integer, HashSet<String>> map = new HashMap<>();

	public static void main(String[] args) throws FileNotFoundException{
		String input = "";
		Scanner scanner = new Scanner(new File("src/Lab_10_Zadanie_3.txt"));
		while(scanner.hasNextLine()){
			input = scanner.nextLine();
			String[] input_splitted = input.split(" ");
			for(String word : input_splitted){
				if(word.length() > 0){
					int h = word.hashCode();
					if(map.containsKey(h)){
						map.get(h).add(word + " ");
					}
					else{
						HashSet<String> set = new HashSet<>();
						set.add(word);
						map.put(h, set);
					}
				}
			}
		}
		for(Map.Entry<Integer, HashSet<String>> map : map.entrySet()){
			int h = map.getKey();
			HashSet<String> set = map.getValue();
			if(set.size() > 1){
				System.out.println(set);
			}
		}
	}
}
