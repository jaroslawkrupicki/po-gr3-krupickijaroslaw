package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium13;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Zadanie_2{

	private static TreeMap<String, String> studenci = new TreeMap<>();

	public static void dodaj(String nazwisko, String ocena){
		String a = nazwisko.substring(0, 1).toUpperCase();
		String b = nazwisko.substring(1).toLowerCase();
		nazwisko = a + b;
		if(studenci.containsKey(nazwisko)){
			System.out.println("Student o podanym nazwisku juz istnieje.");
			return;
		}
		studenci.put(nazwisko, ocena.toLowerCase());
	}

	public static void usun(String nazwisko){
		String a = nazwisko.substring(0, 1).toUpperCase();
		String b = nazwisko.substring(1).toLowerCase();
		nazwisko = a + b;
		if(!studenci.containsKey(nazwisko)){
			System.out.println("Student o podanym nazwisku nie istnieje.");
			return;
		}
		studenci.remove(nazwisko);
	}

	public static void zmienOcene(String nazwisko, String ocena){
		String a = nazwisko.substring(0, 1).toUpperCase();
		String b = nazwisko.substring(1).toLowerCase();
		nazwisko = a + b;
		if(!studenci.containsKey(nazwisko)){
			System.out.println("Student o podanym nazwisku nie istnieje.");
			return;
		}
		studenci.put(nazwisko, ocena.toLowerCase());
	}

	public static void wypisz(){
		System.out.println("#### LISTA ####");
		for(Map.Entry<String, String> student : studenci.entrySet()){
			System.out.println(student.getKey() + ": " + student.getValue());
		}
		System.out.println("###############");
	}

	public static void main(String[] args){
		while(true){
			String input = "";
			Scanner scanner = new Scanner(System.in);
			if(scanner.hasNextLine()){
				input = scanner.nextLine();
			}
			String[] input_splitted = input.split(" ");
			if(input_splitted[0].equalsIgnoreCase("dodaj")){
				if(input_splitted.length >= 3){
					dodaj(input_splitted[1], input_splitted[2]);
				}
			}
			else if(input_splitted[0].equalsIgnoreCase("usun")){
				if(input_splitted.length >= 2){
					usun(input_splitted[1]);
				}
			}
			else if(input_splitted[0].equalsIgnoreCase("zmien")){
				if(input_splitted.length >= 3){
					zmienOcene(input_splitted[1], input_splitted[2]);
				}
			}
			else if(input_splitted[0].equalsIgnoreCase("zakoncz")){
				return;
			}
			wypisz();
		}
	}
}
