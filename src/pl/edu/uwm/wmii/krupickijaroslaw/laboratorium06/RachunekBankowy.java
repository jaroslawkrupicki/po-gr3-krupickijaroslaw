package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium06;

public class RachunekBankowy {

    private static double rocznaStopaProcentowa;
    private double saldo;

    public RachunekBankowy(){
        saldo = 0;
    }

    public RachunekBankowy(double saldoPoczatkowe){
        saldo = saldoPoczatkowe;
    }

    public double saldo(){
        return saldo;
    }

    public void obliczMiesieczneOdsetki(){
        saldo += saldo * rocznaStopaProcentowa / 12;
    }

    public static void setRocznaStopaProcentowa(double rocznaStopaProcentowa){
        RachunekBankowy.rocznaStopaProcentowa = rocznaStopaProcentowa;
    }
}
