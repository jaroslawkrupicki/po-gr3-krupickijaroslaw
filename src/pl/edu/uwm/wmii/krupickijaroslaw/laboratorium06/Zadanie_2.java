package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium06;

public class Zadanie_2 {

    public static void main(String[] args) {
        IntegerSet is1 = new IntegerSet();
        is1.insertElement(3);
        is1.insertElement(64);
        is1.insertElement(37);
        IntegerSet is2 = new IntegerSet();
        is2.insertElement(59);
        is2.insertElement(8);
        is2.insertElement(64);
        is2.insertElement(3);
        is2.insertElement(74);
        System.out.println("is1: " + is1.toString());
        System.out.println("is2: " + is2.toString());
        System.out.println("is1 equals is2: " + is1.equals(is2));
        is2.deleteElement(74);
        System.out.println("is2: " + is2.toString());
        System.out.println("union: " + IntegerSet.union(is1, is2).toString());
        System.out.println("intersection: " + IntegerSet.intersection(is1, is2).toString());
    }
}
