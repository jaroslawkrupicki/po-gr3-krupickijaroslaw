package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium06;

public class IntegerSet {

    private boolean[] zbior;

    public IntegerSet(){
        zbior = new boolean[100];
    }

    public static IntegerSet union(IntegerSet a, IntegerSet b){
        IntegerSet integerSet = new IntegerSet();
        for(int i = 0; i < a.zbior.length; i++){
            if(a.zbior[i]) integerSet.insertElement(i + 1);
            else if(b.zbior[i]) integerSet.insertElement(i + 1);
        }
        return integerSet;
    }

    public static IntegerSet intersection(IntegerSet a, IntegerSet b){
        IntegerSet integerSet = new IntegerSet();
        for(int i = 0; i < a.zbior.length; i++){
            if(a.zbior[i] && b.zbior[i]) integerSet.insertElement(i + 1);
        }
        return integerSet;
    }

    public void insertElement(int i){
        zbior[i - 1] = true;
    }

    public void deleteElement(int i){
        zbior[i - 1] = false;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < zbior.length; i++){
            if(zbior[i]) sb.append((i + 1) + " ");
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object object){
        if(this == object) return true;
        if(!(object instanceof IntegerSet)) return false;
        IntegerSet integerSet = (IntegerSet)object;
        if(zbior.length != integerSet.zbior.length) return false;
        for(int i = 0; i < zbior.length; i++){
            if(zbior[i] != integerSet.zbior[i]) return false;
        }
        return true;
    }
}
