package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium05;

import java.util.ArrayList;

public class Zadanie_2 {

    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        ArrayList<Integer> b = new ArrayList<>();
        b.add(9);
        b.add(7);
        b.add(4);
        b.add(9);
        b.add(11);
        ArrayList<Integer> c = merge(a, b);
        for(int i = 0; i < c.size(); i++){
            System.out.print(c.get(i) + " ");
        }
    }

    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> c = new ArrayList<>();
        int i = 0;
        int j = 0;
        while(i < a.size() && j < b.size()){
            c.add(a.get(i));
            i++;
            c.add(b.get(j));
            j++;
        }
        while(i < a.size()){
            c.add(a.get(i));
            i++;
        }
        while(j < b.size()){
            c.add(b.get(j));
            j++;
        }
        return c;
    }
}
