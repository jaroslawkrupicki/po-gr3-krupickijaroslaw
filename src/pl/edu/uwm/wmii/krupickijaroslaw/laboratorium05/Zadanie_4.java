package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium05;

import java.util.ArrayList;

public class Zadanie_4 {

    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        ArrayList<Integer> c = reversed(a);
        for(int i = 0; i < c.size(); i++){
            System.out.print(c.get(i) + " ");
        }
    }

    public static ArrayList<Integer> reversed(ArrayList<Integer> a){
        ArrayList<Integer> c = new ArrayList<>();
        for(int i = a.size() - 1; i >= 0; i--){
            c.add(a.get(i));
        }
        return c;
    }
}
