package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium05;

import java.util.ArrayList;

public class Zadanie_1 {

    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        ArrayList<Integer> b = new ArrayList<>();
        b.add(9);
        b.add(8);
        b.add(4);
        b.add(9);
        b.add(11);
        ArrayList<Integer> c = append(a, b);
        for(int i = 0; i < c.size(); i++){
            System.out.print(c.get(i) + " ");
        }
    }

    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> c = new ArrayList<>();
        c.addAll(a);
        c.addAll(b);
        return c;
    }
}
