package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium05;

import java.util.ArrayList;

public class Zadanie_5 {

    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        reverse(a);
        for(int i = 0; i < a.size(); i++){
            System.out.print(a.get(i) + " ");
        }
    }

    public static void reverse(ArrayList<Integer> a){
        for(int i = 0; i < a.size() / 2; i++){
            int temp = a.get(i);
            a.set(i, a.get(a.size() - 1 - i));
            a.set(a.size() - 1 - i, temp);
        }
    }
}
