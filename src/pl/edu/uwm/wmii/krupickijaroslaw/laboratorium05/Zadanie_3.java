package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium05;

import java.util.ArrayList;

public class Zadanie_3 {

    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(4);
        a.add(9);
        a.add(16);
        ArrayList<Integer> b = new ArrayList<>();
        b.add(4);
        b.add(7);
        b.add(9);
        b.add(9);
        b.add(11);
        ArrayList<Integer> c = mergeSorted(a, b);
        for(int i = 0; i < c.size(); i++){
            System.out.print(c.get(i) + " ");
        }
    }

    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> c = new ArrayList<>();
        int i = 0;
        int j = 0;
        while(i < a.size() && j < b.size()){
            if(a.get(i) < b.get(j)){
                c.add(a.get(i));
                i++;
            }
            else{
                c.add(b.get(j));
                j++;
            }
        }
        while(i < a.size()){
            c.add(a.get(i));
            i++;
        }
        while(j < b.size()){
            c.add(b.get(j));
            j++;
        }
        return c;
    }
}
