package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium02;

public class Zadanie_1_G {

    public static void main(String[] args) {
        int n = 8;
        int lewy = 1;
        int prawy = 3;
        System.out.println("Lewy: " + lewy);
        System.out.println("Prawy: " + prawy);
        int[] l = {-24, 0, 52, 91, -38, 62, 91, -3};
        for(int i = 0; i < n; i++) {
            if(i == n - 1) System.out.println(l[i]);
            else System.out.print(l[i] + ", ");
        }
        for(int i = 0; i < (prawy - lewy) / 2; i++) {
            int a = l[lewy + i];
            l[lewy + i] = l[prawy - i];
            l[prawy - i] = a;
        }
        for(int i = 0; i < n; i++) {
            if(i == n - 1) System.out.println(l[i]);
            else System.out.print(l[i] + ", ");
        }
    }
}
