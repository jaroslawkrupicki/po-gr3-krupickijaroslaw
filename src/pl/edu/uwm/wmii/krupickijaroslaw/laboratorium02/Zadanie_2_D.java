package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium02;

import java.util.Random;

public class Zadanie_2_D {

    public static void main(String[] args) {
        int n = 8;
        int[] tab = new int[n];
        generuj(tab, n, -999, 999);
        System.out.println("Suma dodatnich: " + sumaDodatnich(tab));
        System.out.println("Suma ujemnych: " + sumaUjemnych(tab));
    }

    public static void generuj(int[] tab, int n, int minWartosc, int maxWartosc){
        Random rand = new Random();
        for(int i = 0; i < n; i++){
            tab[i] = rand.nextInt((maxWartosc - minWartosc) + 1) + minWartosc;
        }
    }

    public static int sumaDodatnich(int[] tab){
        int wynik = 0;
        for(int a : tab){
            if(a > 0) wynik += a;
        }
        return wynik;
    }

    public static int sumaUjemnych(int[] tab){
        int wynik = 0;
        for(int a : tab){
            if(a < 0) wynik += a;
        }
        return wynik;
    }
}
