package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium02;

import java.util.Random;

public class Zadanie_2_F {

    public static void main(String[] args) {
        int n = 8;
        int[] tab = new int[n];
        generuj(tab, n, -999, 999);
        signum(tab);
        for(int i = 0; i < n; i++) {
            if(i == n - 1) System.out.print(tab[i]);
            else System.out.print(tab[i] + ", ");
        }
    }

    public static void generuj(int[] tab, int n, int minWartosc, int maxWartosc){
        Random rand = new Random();
        for(int i = 0; i < n; i++){
            tab[i] = rand.nextInt((maxWartosc - minWartosc) + 1) + minWartosc;
        }
    }

    public static void signum(int[] tab){
        for(int i = 0; i < tab.length; i++){
            if(tab[i] > 0) tab[i] = 1;
            else if(tab[i] < 0) tab[i] = -1;
        }
    }
}
