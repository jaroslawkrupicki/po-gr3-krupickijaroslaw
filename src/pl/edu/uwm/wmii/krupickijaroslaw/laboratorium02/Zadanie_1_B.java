package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium02;

public class Zadanie_1_B {

    public static void main(String[] args) {
        int n = 8;
        int[] l = {-24, 0, 52, -83, -38, 62, 91, -3};
        int ujemne = 0;
        int dodatnie = 0;
        int zero = 0;
        for(int i = 0; i < n; i++){
            if(l[i] < 0) ujemne++;
            else if(l[i] > 0) dodatnie++;
            else zero++;
        }
        System.out.println("Ujemne: " + ujemne);
        System.out.println("Dodatnie: " + dodatnie);
        System.out.println("Zero: " + zero);
    }
}
