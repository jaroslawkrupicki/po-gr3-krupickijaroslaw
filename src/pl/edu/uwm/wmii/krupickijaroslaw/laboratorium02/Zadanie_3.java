package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium02;

public class Zadanie_3 {

    public static void main(String[] args) {
        int m = 3;
        int n = 3;
        int k = 3;
        // m * n
        int[][] a = {
                {3, 5, 4},
                {6, 2, 7},
                {1, 9, 3}
        };
        // n * k
        int[][] b = {
                {2, 9, 7},
                {6, 4, 5},
                {2, 2, 1}
        };
        System.out.println("Macierz a:");
        wypiszMacierz(a, m, n);
        System.out.println("Macierz b:");
        wypiszMacierz(b, n, k);
        int[][] c = new int[m][k];
        for(int i = 0; i < n; i++){
            for(int j = 0; j < m; j++){
                for(int l = 0; l < k; l++) {
                    c[i][j] += a[i][l] * b[l][j];
                }
            }
        }
        System.out.println("Macierz c:");
        wypiszMacierz(c, m, k);
    }

    public static void wypiszMacierz(int[][] tab, int a, int b){
        for(int i = 0; i < a; i++){
            for(int j = 0; j < b; j++){
                System.out.print(tab[i][j] + " ");
            }
            System.out.print("\n");
        }
    }
}
