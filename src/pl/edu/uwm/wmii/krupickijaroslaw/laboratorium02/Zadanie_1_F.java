package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium02;

public class Zadanie_1_F {

    public static void main(String[] args) {
        int n = 8;
        int[] l = {-24, 0, 52, 91, -38, 62, 91, -3};
        for(int i = 0; i < n; i++){
            if(l[i] > 0){
                l[i] = 1;
            }
            else if(l[i] < 0){
                l[i] = -1;
            }
        }
        for(int i = 0; i < n; i++) {
            if(i == n - 1) System.out.print(l[i]);
            else System.out.print(l[i] + ", ");
        }
    }
}
