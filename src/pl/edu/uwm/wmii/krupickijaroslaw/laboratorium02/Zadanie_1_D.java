package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium02;

public class Zadanie_1_D {

    public static void main(String[] args) {
        int n = 8;
        int[] l = {-24, 0, 52, 91, -38, 62, 91, -3};
        int suma_ujemnych = 0;
        int suma_dodatnich = 0;
        for(int i = 0; i < n; i++){
            if(l[i] < 0) suma_ujemnych += l[i];
            else if(l[i] > 0) suma_dodatnich += l[i];
        }
        System.out.println("Suma ujemnych: " + suma_ujemnych);
        System.out.println("Suma dodatnich: " + suma_dodatnich);
    }
}
