package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium02;

import java.util.Random;

public class Zadanie_2_G {

    public static void main(String[] args) {
        int n = 8;
        int[] tab = new int[n];
        generuj(tab, n, -999, 999);
        int lewy = 1;
        int prawy = 3;
        for(int i = 0; i < n; i++) {
            if(i == n - 1) System.out.println(tab[i]);
            else System.out.print(tab[i] + ", ");
        }
        odwrocFragment(tab, lewy, prawy);
        for(int i = 0; i < n; i++) {
            if(i == n - 1) System.out.println(tab[i]);
            else System.out.print(tab[i] + ", ");
        }
    }

    public static void generuj(int[] tab, int n, int minWartosc, int maxWartosc){
        Random rand = new Random();
        for(int i = 0; i < n; i++){
            tab[i] = rand.nextInt((maxWartosc - minWartosc) + 1) + minWartosc;
        }
    }

    public static void odwrocFragment(int[] tab, int lewy, int prawy){
        for(int i = 0; i < (prawy - lewy) / 2; i++) {
            int a = tab[lewy + i];
            tab[lewy + i] = tab[prawy - i];
            tab[prawy - i] = a;
        }
    }
}
