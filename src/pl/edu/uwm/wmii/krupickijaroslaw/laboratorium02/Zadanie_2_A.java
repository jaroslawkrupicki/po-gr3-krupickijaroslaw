package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium02;

import java.util.Random;

public class Zadanie_2_A {

    public static void main(String[] args) {
        int n = 8;
        int[] tab = new int[n];
        generuj(tab, n, -999, 999);
        System.out.println("Nieparzyste: " + ileNieparzystych(tab));
        System.out.println("Parzyste: " + ileParzystych(tab));
    }

    public static void generuj(int[] tab, int n, int minWartosc, int maxWartosc){
        Random rand = new Random();
        for(int i = 0; i < n; i++){
            tab[i] = rand.nextInt((maxWartosc - minWartosc) + 1) + minWartosc;
        }
    }

    public static int ileNieparzystych(int[] tab){
        int wynik = 0;
        for(int a : tab){
            if(!(a % 2 == 0)) wynik++;
        }
        return wynik;
    }

    public static int ileParzystych(int[] tab){
        int wynik = 0;
        for(int a : tab){
            if(a % 2 == 0) wynik++;
        }
        return wynik;
    }
}
