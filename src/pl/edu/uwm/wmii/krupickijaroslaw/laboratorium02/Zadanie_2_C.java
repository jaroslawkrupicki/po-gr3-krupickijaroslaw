package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium02;

import java.util.Random;

public class Zadanie_2_C {

    public static void main(String[] args) {
        int n = 8;
        int[] tab = new int[n];
        generuj(tab, n, -999, 999);
        System.out.println(ileMaksymalnych(tab));
    }

    public static void generuj(int[] tab, int n, int minWartosc, int maxWartosc){
        Random rand = new Random();
        for(int i = 0; i < n; i++){
            tab[i] = rand.nextInt((maxWartosc - minWartosc) + 1) + minWartosc;
        }
    }

    public static int ileMaksymalnych(int[] tab){
        int max = tab[0];
        int wynik = 1;
        for(int i = 1; i < tab.length; i++){
            if(tab[i] > max){
                max = tab[i];
                wynik = 1;
            }
            else if(tab[i] == max){
                wynik++;
            }
        }
        return wynik;
    }
}
