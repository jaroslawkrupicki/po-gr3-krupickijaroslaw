package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium02;

public class Zadanie_1_A {

    public static void main(String[] args) {
        int n = 8;
        int[] l = {-24, 0, 52, -83, -38, 62, 91, -3};
        int nieparzyste = 0;
        int parzyste = 0;
        for(int i = 0; i < n; i++){
            if(l[i] % 2 == 0) parzyste++;
            else nieparzyste++;
        }
        System.out.println("Nieparzyste: " + nieparzyste);
        System.out.println("Parzyste: " + parzyste);
    }
}
