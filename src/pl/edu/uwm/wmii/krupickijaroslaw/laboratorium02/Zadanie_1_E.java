package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium02;

public class Zadanie_1_E {

    public static void main(String[] args) {
        int n = 8;
        int[] l = {-24, 0, 52, 91, -38, 62, 91, -3};
        int a = 0;
        int b = 0;
        for(int i = 0; i < n; i++){
            if(l[i] > 0) b++;
            else if(b > a){
                a = b;
                b = 0;
            }
        }
        if(b > a) a = b;
        System.out.println(a);
    }
}
