package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium02;

public class Zadanie_1_C {

    public static void main(String[] args) {
        int n = 8;
        int[] l = {-24, 0, 52, 91, -38, 62, 91, -3};
        int max = l[0];
        int m = 1;
        for(int i = 1; i < n; i++){
            if(l[i] > max){
                max = l[i];
                m = 1;
            }
            else if(l[i] == max){
                m++;
            }
        }
        System.out.println("Najwiekszy: " + max);
        System.out.println("Ilosc: " + m);
    }
}
