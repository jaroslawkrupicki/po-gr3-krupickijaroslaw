package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium00;

public class Zadanie11 {

    public static void main(String[] args) {
        System.out.println("Dłoń, Wiesława Szymborska");
        System.out.println();
        System.out.println("Dwadzieścia siedem kości,");
        System.out.println("trzydzieści pięć mięśni,");
        System.out.println("około dwóch tysięcy komórek nerwowych");
        System.out.println("w każdej opuszce naszych pięciu palców.");
        System.out.println("To zupełnie wystarczy,");
        System.out.println("żeby napisać \"Mein Kampf\"");
        System.out.println("albo \"Chatkę Puchatka\".");
    }
}
