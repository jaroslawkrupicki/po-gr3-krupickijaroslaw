package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium00;

public class Zadanie4 {

    public static void main(String[] args) {
        int saldo = 1000;
        saldo *= 1.06;
        System.out.println("Po 1 roku: " + saldo);
        saldo *= 1.06;
        System.out.println("Po 2 roku: " + saldo);
        saldo *= 1.06;
        System.out.println("Po 3 roku: " + saldo);
    }
}
