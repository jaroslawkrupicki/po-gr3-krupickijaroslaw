package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium10;

import java.time.LocalDate;

public class Osoba implements Cloneable, Comparable<Osoba>{

	private String nazwisko;
	private LocalDate dataUrodzenia;

	public Osoba(String nazwisko, LocalDate dataUrodzenia){
		this.nazwisko = nazwisko;
		this.dataUrodzenia = dataUrodzenia;
	}

	public String getNazwisko(){
		return nazwisko;
	}

	public LocalDate getDataUrodzenia(){
		return dataUrodzenia;
	}

	@Override
	public int compareTo(Osoba osoba){
		int n = nazwisko.compareTo(osoba.nazwisko);
		if(n == 0){
			if(dataUrodzenia.isEqual(osoba.dataUrodzenia)) return 0;
			if(dataUrodzenia.isBefore(osoba.dataUrodzenia)) return -1;
			return 1;
		}
		return n;
	}

	@Override
	public String toString(){
		return getClass().getSimpleName() + "[" + nazwisko + "|" + dataUrodzenia.toString() + "]";
	}

	@Override
	public boolean equals(Object object){
		if(this == object) return true;
		if(!(object instanceof Osoba)) return false;
		Osoba obj = (Osoba)object;
		if(!nazwisko.equalsIgnoreCase(obj.nazwisko)) return false;
		if(!dataUrodzenia.isEqual(obj.dataUrodzenia)) return false;
		return true;
	}
}
