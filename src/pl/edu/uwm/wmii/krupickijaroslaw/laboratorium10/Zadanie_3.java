package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium10;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Zadanie_3{

	public static void main(String[] args){
		ArrayList<String> a = new ArrayList<>();
		try{
			String file = args[0];
			//String file = "src/Lab_10_Zadanie_3.txt";
			Scanner scanner = new Scanner(new File(file));
			while(scanner.hasNextLine()){
				String line = scanner.nextLine();
				a.add(line);
			}
		}
		catch(FileNotFoundException e){
			e.printStackTrace();
		}
		a.sort(String::compareTo);
		for(String s : a){
			System.out.println(s);
		}
	}
}
