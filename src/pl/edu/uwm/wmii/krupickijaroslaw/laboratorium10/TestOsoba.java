package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium10;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestOsoba{

	public static void main(String[] args){
		ArrayList<Osoba> grupa = new ArrayList<>();
		grupa.add(new Osoba("Nowak", LocalDate.of(1998, 10, 26)));
		grupa.add(new Osoba("Kowalski", LocalDate.of(2004, 4, 15)));
		grupa.add(new Osoba("Nowak", LocalDate.of(1996, 7, 2)));
		grupa.add(new Osoba("Ziobro", LocalDate.of(2004, 4, 15)));
		grupa.add(new Osoba("Krupicki", LocalDate.of(1999, 8, 6)));
		System.out.println(grupa.get(0).toString());
		System.out.println(grupa.get(0).equals(grupa.get(1)));
		System.out.println("Przed sortowaniem:");
		for(Osoba o : grupa){
			System.out.println(o.toString());
		}
		grupa.sort(Osoba::compareTo);
		System.out.println("Po sortowaniu:");
		for(Osoba o : grupa){
			System.out.println(o.toString());
		}
	}
}
