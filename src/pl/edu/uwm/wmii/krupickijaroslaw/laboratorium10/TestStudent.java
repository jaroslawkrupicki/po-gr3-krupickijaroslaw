package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium10;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestStudent{

	public static void main(String[] args){
		ArrayList<Student> grupa = new ArrayList<>();
		grupa.add(new Student("Nowak", LocalDate.of(1998, 10, 26), 4.9));
		grupa.add(new Student("Kowalski", LocalDate.of(2004, 4, 15), 3.5));
		grupa.add(new Student("Nowak", LocalDate.of(1996, 7, 2), 3.6));
		grupa.add(new Student("Ziobro", LocalDate.of(2004, 4, 15), 2.7));
		grupa.add(new Student("Krupicki", LocalDate.of(1999, 8, 6), 4.4));
		//grupa.add(new Student("Krupicki", LocalDate.of(1999, 8, 6), 4.3));
		System.out.println("Przed sortowaniem:");
		for(Student o : grupa){
			System.out.println(o.toString());
		}
		grupa.sort(Student::compareTo);
		System.out.println("Po sortowaniu:");
		for(Student o : grupa){
			System.out.println(o.toString());
		}
	}
}
