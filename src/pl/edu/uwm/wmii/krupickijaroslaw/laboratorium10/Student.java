package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium10;

import java.time.LocalDate;

public class Student extends Osoba implements Cloneable, Comparable<Osoba>{

	private double sredniaOcen;

	public Student(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen){
		super(nazwisko, dataUrodzenia);
		this.sredniaOcen = sredniaOcen;
	}

	public int compareTo(Student student){
		int n = super.compareTo(student);
		if(n == 0){
			if(sredniaOcen == student.sredniaOcen) return 0;
			if(sredniaOcen < student.sredniaOcen) return -1;
			return 1;
		}
		return n;
	}

	@Override
	public String toString(){
		return getClass().getSimpleName() + "[" + getNazwisko() + "|" + getDataUrodzenia().toString() + "|" + sredniaOcen + "]";
	}
}
