package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium01;

public class Zadanie_2_1_H {

    public static void main(String[] args) {
        int n = 3;
        double[] l = {4, 5, -7};
        int wynik = 0;
        for(int k = 1; k <= n; k++){
            if(Math.abs(l[k-1]) < Math.pow(k, 2)) wynik++;
        }
        System.out.println(wynik);
    }
}
