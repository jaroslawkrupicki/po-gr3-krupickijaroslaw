package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium01;

public class Zadanie_2_3 {

    public static void main(String[] args) {
        int n = 3;
        double[] l = {4, 3, -7};
        int dodatnie = 0;
        int ujemne = 0;
        int zero = 0;
        for(int i = 0; i < n; i++){
            if(l[i] > 0) dodatnie++;
            else if(l[i] < 0) ujemne++;
            else zero++;
        }
        System.out.println("Dodatnie: " + dodatnie);
        System.out.println("Ujemne: " + ujemne);
        System.out.println("Zero: " + zero);
    }
}
