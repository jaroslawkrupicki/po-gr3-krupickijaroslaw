package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium01;

public class Zadanie_2_1_B {

    public static void main(String[] args) {
        int n = 5;
        double[] l = {15, -18, 12, 0, 10};
        int wynik = 0;
        for(int i = 0; i < n; i++){
            if(l[i] % 3 == 0 && l[i] % 5 != 0) wynik++;
        }
        System.out.println(wynik);
    }
}
