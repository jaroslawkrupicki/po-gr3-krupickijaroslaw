package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium01;

public class Zadanie_1_1_D {

    public static void main(String[] args) {
        int n = 3;
        double[] l = {-25, -9, 16};
        double wynik = 0;
        for(int i = 0; i < n; i++){
            wynik += Math.sqrt(Math.abs(l[i]));
        }
        System.out.println(wynik);
    }
}
