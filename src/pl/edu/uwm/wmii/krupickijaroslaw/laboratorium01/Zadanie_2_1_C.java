package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium01;

public class Zadanie_2_1_C {

    public static void main(String[] args) {
        int n = 3;
        double[] l = {16, -27, 25};
        int wynik = 0;
        for(int i = 0; i < n; i++){
            double pierwiastek = Math.sqrt(l[i]);
            if((int)pierwiastek == pierwiastek && pierwiastek % 2 == 0) wynik++;
        }
        System.out.println(wynik);
    }
}
