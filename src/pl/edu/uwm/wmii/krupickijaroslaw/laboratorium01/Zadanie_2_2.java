package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium01;

public class Zadanie_2_2 {

    public static void main(String[] args) {
        int n = 3;
        double[] l = {4, 5, -7};
        double wynik = 0;
        for(int i = 0; i < n; i++){
            if(l[i] > 0) wynik += l[i] * 2;
        }
        System.out.println(wynik);
    }
}
