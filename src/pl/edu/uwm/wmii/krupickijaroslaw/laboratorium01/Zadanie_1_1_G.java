package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium01;

public class Zadanie_1_1_G {

    public static void main(String[] args) {
        int n = 3;
        double[] l = {2, 3, 4};
        double wynik = 0;
        for(int i = 0; i < n; i++){
            wynik += l[i];
        }
        System.out.println(wynik);
        wynik = 1;
        for(int i = 0; i < n; i++){
            wynik *= l[i];
        }
        System.out.println(wynik);
    }
}
