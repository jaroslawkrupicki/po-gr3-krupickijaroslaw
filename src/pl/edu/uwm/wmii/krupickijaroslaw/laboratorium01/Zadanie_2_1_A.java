package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium01;

public class Zadanie_2_1_A {

    public static void main(String[] args) {
        int n = 3;
        double[] l = {2, 3, 4};
        int wynik = 0;
        for(int i = 0; i < n; i++){
            if(l[i] % 2 == 1) wynik++;
        }
        System.out.println(wynik);
    }
}
