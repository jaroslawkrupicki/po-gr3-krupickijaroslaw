package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium01;

public class Zadanie_2_5 {

    public static void main(String[] args) {
        int n = 6;
        double[] l = {3, 5, 2, -4, 9, 7};
        int wynik = 0;
        for(int i = 1; i < n; i++){
            if(l[i-1] > 0 && l[i] > 0) wynik++;
        }
        System.out.println(wynik);
    }
}
