package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium01;

public class Zadanie_2_1_F {

    public static void main(String[] args) {
        int n = 3;
        double[] l = {4, 5, -8};
        int wynik = 0;
        for(int k = 1; k <= n; k+=2){
            if(l[k-1] % 2 == 0) wynik++;
        }
        System.out.println(wynik);
    }
}
