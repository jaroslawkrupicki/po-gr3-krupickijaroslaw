package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium01;

public class Zadanie_2_4 {

    public static void main(String[] args) {
        int n = 6;
        double[] l = {4, 3, -7, 5, -3, -8};
        double min = l[0];
        double max = l[0];
        for(int i = 1; i < n; i++){
            if(l[i] < min) min = l[i];
            else if(l[i] > max) max = l[i];
        }
        System.out.println("Min: " + min);
        System.out.println("Max: " + max);
    }
}
