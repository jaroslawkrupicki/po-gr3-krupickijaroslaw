package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium01;

public class Zadanie_2_1_G {

    public static void main(String[] args) {
        int n = 3;
        double[] l = {4, 5, -7};
        int wynik = 0;
        for(int i = 0; i < n; i++){
            if(!(l[i] % 2 == 0) && l[i] >= 0) wynik++;
        }
        System.out.println(wynik);
    }
}
