package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium01;

public class Zadanie_2_1_D {

    public static void main(String[] args) {
        int n = 5;
        double[] l = {4, 5, -7, 24, 8};
        int wynik = 0;
        for(int k = 1; k < n-1; k++){
            if(l[k] < (l[k-1] + l[k+1]) / 2) wynik++;
        }
        System.out.println(wynik);
    }
}
