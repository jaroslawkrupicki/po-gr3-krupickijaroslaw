package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium01;

public class Zadanie_1_1_I {

    public static void main(String[] args) {
        int n = 3;
        double[] l = {2, 3, 4};
        double wynik = 0;
        for(int i = 0; i < n; i++){
            wynik += (l[i] * Math.pow(-1, i + 1)) / silnia(i + 1);
        }
        System.out.println(wynik);
    }

    public static int silnia(int a){
        if(a == 0 || a == 1) return 1;
        return a * silnia(a - 1);
    }
}
