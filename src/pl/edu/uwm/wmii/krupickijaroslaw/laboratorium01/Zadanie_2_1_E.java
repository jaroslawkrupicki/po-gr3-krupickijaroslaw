package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium01;

public class Zadanie_2_1_E {

    public static void main(String[] args) {
        int n = 5;
        double[] l = {4, 5, -7, 20, 100};
        int wynik = 0;
        for(int k = 1; k <= n; k++){
            if(Math.pow(2, k) < l[k-1] && l[k-1] < silnia(k)) wynik++;
        }
        System.out.println(wynik);
    }

    public static int silnia(int a){
        if(a == 0 || a == 1) return 1;
        return a * silnia(a - 1);
    }
}
