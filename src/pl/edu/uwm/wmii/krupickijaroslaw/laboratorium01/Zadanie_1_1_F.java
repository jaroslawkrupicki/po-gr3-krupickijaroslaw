package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium01;

public class Zadanie_1_1_F {

    public static void main(String[] args) {
        int n = 3;
        double[] l = {2, 3, 4};
        double wynik = 0;
        for(int i = 0; i < n; i++){
            wynik += Math.pow(l[i], 2);
        }
        System.out.println(wynik);
    }
}
