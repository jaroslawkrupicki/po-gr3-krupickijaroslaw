package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium01;

public class Zadanie_1_1_E {

    public static void main(String[] args) {
        int n = 3;
        double[] l = {-2, -3, 4};
        double wynik = 1;
        for(int i = 0; i < n; i++){
            wynik *= Math.abs(l[i]);
        }
        System.out.println(wynik);
    }
}
