package pl.edu.uwm.wmii.krupickijaroslaw.test2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;

public class Main{

	public static void main(String[] args){
		//###############
		// ZADANIE 1 - KOMPUTERY
		//###############
		System.out.println();
		System.out.println("Zadanie 1 - KOMPUTERY:");
		System.out.println();
		ArrayList<Komputer> grupa = new ArrayList<>();
		grupa.add(new Komputer("PC-1", LocalDate.of(2003, 6, 24)));
		grupa.add(new Komputer("PC-1", LocalDate.of(2002, 8, 15)));
		grupa.add(new Komputer("PC-3", LocalDate.of(2005, 2, 9)));
		grupa.add(new Komputer("PC-2", LocalDate.of(2005, 2, 9)));
		grupa.add(new Komputer("PC-1", LocalDate.of(2004, 5, 1)));
		for(Komputer komputer : grupa){
			System.out.println(komputer.getNazwa() + " | " + komputer.getDataProdukcji());
		}
		grupa.sort(Komputer::compareTo);
		System.out.println("Po sortowaniu:");
		for(Komputer komputer : grupa){
			System.out.println(komputer.getNazwa() + " | " + komputer.getDataProdukcji());
		}
		//###############
		// ZADANIE 1 - LAPTOPY
		//###############
		System.out.println();
		System.out.println("Zadanie 1 - LAPTOPY:");
		System.out.println();
		ArrayList<Komputer> grupaLaptopow = new ArrayList<>();
		grupaLaptopow.add(new Laptop("L-1", LocalDate.of(2003, 6, 24), false));
		grupaLaptopow.add(new Laptop("L-1", LocalDate.of(2002, 8, 15), false));
		grupaLaptopow.add(new Laptop("L-3", LocalDate.of(2005, 2, 9), false));
		grupaLaptopow.add(new Laptop("L-2", LocalDate.of(2005, 2, 9), false));
		grupaLaptopow.add(new Laptop("L-3", LocalDate.of(2005, 2, 9), true));
		for(Komputer komputer : grupaLaptopow){
			if(komputer instanceof Laptop){
				Laptop laptop = (Laptop)komputer;
				System.out.println(laptop.getNazwa() + " | " + laptop.getDataProdukcji() + " | " + laptop.czyApple());
			}
			else System.out.println(komputer.getNazwa() + " | " + komputer.getDataProdukcji());

		}
		grupaLaptopow.sort(Komputer::compareTo);
		System.out.println("Po sortowaniu:");
		for(Komputer komputer : grupaLaptopow){
			if(komputer instanceof Laptop){
				Laptop laptop = (Laptop)komputer;
				System.out.println(laptop.getNazwa() + " | " + laptop.getDataProdukcji() + " | " + laptop.czyApple());
			}
			else System.out.println(komputer.getNazwa() + " | " + komputer.getDataProdukcji());

		}
		//###############
		// ZADANIE 2
		//###############
		System.out.println();
		System.out.println("Zadanie 2:");
		System.out.println();
		int n = 3;
		LinkedList<String> komputery = new LinkedList<>();
		komputery.add("PC-1");
		komputery.add("PC-2");
		komputery.add("PC-3");
		komputery.add("PC-4");
		komputery.add("PC-5");
		komputery.add("PC-6");
		komputery.add("PC-7");
		komputery.add("PC-8");
		for(String komputer : komputery){
			System.out.println(komputer);
		}
		redukuj(komputery, n);
		System.out.println("Po redukcji dla n=" + n + ":");
		for(String komputer : komputery){
			System.out.println(komputer);
		}
	}

	public static void redukuj(LinkedList<String> komputery, int n){
		if(n < 1) return;
		int i = n - 1;
		while(i < komputery.size()){
			komputery.remove(i);
			i += n - 1;
		}
	}
}
