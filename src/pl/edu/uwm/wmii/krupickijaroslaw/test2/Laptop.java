package pl.edu.uwm.wmii.krupickijaroslaw.test2;

import java.time.LocalDate;

public class Laptop extends Komputer implements Cloneable, Comparable<Komputer>{

	private boolean czyApple;

	public Laptop(String nazwa, LocalDate dataProdukcji, boolean czyApple){
		super(nazwa, dataProdukcji);
		this.czyApple = czyApple;
	}

	public boolean czyApple(){
		return czyApple;
	}

	@Override
	public int compareTo(Komputer o){
		int i = super.compareTo(o);
		if(i == 0){
			if(o instanceof Laptop){
				Laptop laptop = (Laptop)o;
				if(czyApple == laptop.czyApple) return 0;
				if(czyApple) return -1;
				return 1;
			}
		}
		return i;
	}
}
