package pl.edu.uwm.wmii.krupickijaroslaw.test2;

import java.time.LocalDate;

public class Komputer implements Cloneable, Comparable<Komputer>{

	private String nazwa;
	private LocalDate dataProdukcji;

	public Komputer(String nazwa, LocalDate dataProdukcji){
		this.nazwa = nazwa;
		this.dataProdukcji = dataProdukcji;
	}

	public String getNazwa(){
		return nazwa;
	}

	public LocalDate getDataProdukcji(){
		return dataProdukcji;
	}

	@Override
	public int compareTo(Komputer o){
		if(nazwa.equals(o.nazwa)){
			return dataProdukcji.compareTo(o.dataProdukcji);
		}
		return nazwa.compareTo(o.nazwa);
	}

	@Override
	public boolean equals(Object object){
		if(this == object) return true;
		if(!(object instanceof Komputer)) return false;
		Komputer obj = (Komputer)object;
		if(!nazwa.equals(obj.nazwa)) return false;
		if(!dataProdukcji.isEqual(obj.dataProdukcji)) return false;
		return true;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException{
		Komputer komputer = (Komputer)super.clone();
		komputer.nazwa = nazwa;
		komputer.dataProdukcji = dataProdukcji;
		return komputer;
	}
}
