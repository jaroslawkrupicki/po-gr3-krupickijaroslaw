package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium07;

import pl.imiajd.krupicki.Adres;

public class Zadanie_2 {

    public static void main(String[] args) {
        Adres a1 = new Adres("Zamkowa", 4, 7, "Olsztyn", "11-306");
        Adres a2 = new Adres("Zamkowa", 3, "Olsztyn", "11-305");
        System.out.println("a1 < a2: " + a1.przed(a2));
        System.out.println("a2 < a1: " + a2.przed(a1));
    }
}
