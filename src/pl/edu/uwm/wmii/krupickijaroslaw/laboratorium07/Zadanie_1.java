package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium07;

import pl.imiajd.krupicki.NazwanyPunkt;
import pl.imiajd.krupicki.Punkt;

public class Zadanie_1 {

    public static void main(String[] args) {
        Punkt p1 = new Punkt(5, 3);
        NazwanyPunkt p2 = new NazwanyPunkt(6, 8, "P2");
        p1.show();
        p2.show();
    }
}
