package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium07;

import pl.imiajd.krupicki.BetterRectangle;

public class Zadanie_6 {

    public static void main(String[] args) {
        BetterRectangle br = new BetterRectangle(4, 3);
        System.out.println(br.getPerimeter());
        System.out.println(br.getArea());
    }
}
