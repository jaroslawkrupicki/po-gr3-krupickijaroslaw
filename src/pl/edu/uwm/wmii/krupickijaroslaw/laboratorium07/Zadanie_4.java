package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium07;

import pl.imiajd.krupicki.Nauczyciel;
import pl.imiajd.krupicki.Osoba;
import pl.imiajd.krupicki.Student;

public class Zadanie_4 {

    public static void main(String[] args) {
        Osoba osoba = new Osoba("Kowalski", 2004);
        Student student = new Student("Krupicki", 1999, "informatyka");
        Nauczyciel nauczyciel = new Nauczyciel("Nowak", 1980, 4021);
        System.out.println(osoba.toString());
        System.out.println(student.toString());
        System.out.println(nauczyciel.toString());
    }
}
