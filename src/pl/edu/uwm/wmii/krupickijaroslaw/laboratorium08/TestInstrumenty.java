package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium08;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestInstrumenty {

    public static void main(String[] args) {
        ArrayList<Instrument> orkiestra = new ArrayList<>();
        orkiestra.add(new Flet("Firma A", LocalDate.of(1943, 4, 12)));
        orkiestra.add(new Fortepian("Firma B", LocalDate.of(1953, 12, 30)));
        orkiestra.add(new Skrzypce("Firma C", LocalDate.of(2001, 4, 17)));
        orkiestra.add(new Flet("Firma D", LocalDate.of(1992, 7, 4)));
        orkiestra.add(new Fortepian("Firma A", LocalDate.of(1995, 11, 26)));
        for(Instrument instrument : orkiestra){
            instrument.dzwiek();
        }
        for(Instrument instrument : orkiestra){
            System.out.println(instrument.toString());
        }
    }
}
