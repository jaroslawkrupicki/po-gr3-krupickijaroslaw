package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium08;

import java.time.LocalDate;

public class Pracownik extends Osoba{

    private double pobory;
    private LocalDate dataZatrudnienia;

    public Pracownik(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, double pobory, LocalDate dataZatrudnienia) {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public double getPobory() {
        return pobory;
    }

    public LocalDate getDataZatrudnienia() {
        return dataZatrudnienia;
    }

    @Override
    public String getOpis() {
        return String.format("pracownik z pensją %.2f zł", pobory);
    }
}
