package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium08;

import java.time.LocalDate;
import java.util.Arrays;

public class TestOsoba {

    public static void main(String[] args) {
        Osoba[] ludzie = new Osoba[2];
        ludzie[0] = new Pracownik("Kowalski", new String[]{"Jan"}, LocalDate.of(2001, 10, 24), true,50000, LocalDate.of(2019, 2, 5));
        ludzie[1] = new Student("Nowak", new String[]{"Małgorzata"},LocalDate.of(2003, 6, 16), false, "informatyka", 3.4);
        for(Osoba p : ludzie){
            if(p instanceof Pracownik){
                System.out.println("PRACOWNIK");
                Pracownik pracownik = (Pracownik)p;
                System.out.println(pracownik.getNazwisko());
                System.out.println(Arrays.toString(pracownik.getImiona()));
                System.out.println(pracownik.getDataUrodzenia());
                System.out.println(pracownik.getPlec() ? "mezczyzna" : "kobieta");
                System.out.println(pracownik.getPobory());
                System.out.println(pracownik.getDataZatrudnienia());
            }
            else if(p instanceof Student){
                System.out.println("STUDENT");
                Student student = (Student)p;
                System.out.println(student.getNazwisko());
                System.out.println(Arrays.toString(student.getImiona()));
                System.out.println(student.getDataUrodzenia());
                System.out.println(student.getPlec() ? "mezczyzna" : "kobieta");
                System.out.println(student.getSredniaOcen());
            }
        }
    }
}
