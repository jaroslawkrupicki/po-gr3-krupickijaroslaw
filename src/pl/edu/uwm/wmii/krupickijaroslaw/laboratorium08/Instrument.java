package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium08;

import java.time.LocalDate;

public abstract class Instrument {

    private String producent;
    private LocalDate rokProdukcji;

    public Instrument(String producent, LocalDate rokProdukcji){
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }

    public String getProducent() {
        return producent;
    }

    public LocalDate getRokProdukcji() {
        return rokProdukcji;
    }

    public abstract void dzwiek();

    @Override
    public String toString(){
        return producent + ", " + rokProdukcji;
    }

    @Override
    public boolean equals(Object object){
        if(this == object) return true;
        if(!(object instanceof Instrument)) return false;
        Instrument obj = (Instrument)object;
        if(!producent.equalsIgnoreCase(obj.producent)) return false;
        if(rokProdukcji != obj.rokProdukcji) return false;
        return true;
    }
}
