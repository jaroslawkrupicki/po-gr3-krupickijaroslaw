package pl.edu.uwm.wmii.krupickijaroslaw.test1;

public class Zadanie_2 {

    public static void main(String[] args) {
        String str = "Rabarbar";
        char c = 'a';
        System.out.println(delete(str, c));
    }

    public static String delete(String str, char c){
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for(int i = 0; i < str.length(); i++){
            if(str.charAt(i) == c){
                if(first){
                    sb.append(str.charAt(i));
                    first = false;
                }
            }
            else{
                sb.append(str.charAt(i));
            }
        }
        return sb.toString();
    }
}
