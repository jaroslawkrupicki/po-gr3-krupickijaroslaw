package pl.edu.uwm.wmii.krupickijaroslaw.test1;

public class Test1 {

    public static void main(String[] args) {
        int n = 15;
        double[] a = {5.1,7,2,8,-4.9,-2,-7,-5,-5,34.3,75,-35,-23,43,-5.2};
        zadanie1(n, a);
        String str = "Rabarbar";
        char c = 'a';
        System.out.println(delete(str, c));
    }

    public static void zadanie1(int n, double[] a) {
        int mniejsze = 0;
        int wieksze = 0;
        int rowne = 0;
        for(int i = 0; i < n; i++){
            if(a[i] > 5) wieksze++;
            else if(a[i] < -5) mniejsze++;
            else if(a[i] == -5) rowne++;
        }
        System.out.println("Liczby >5: " + wieksze);
        System.out.println("Liczby <-5: " + mniejsze);
        System.out.println("Liczby =-5: " + rowne);
        System.out.println("Razem: " + (wieksze + mniejsze + rowne));
    }

    public static String delete(String str, char c){
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for(int i = 0; i < str.length(); i++){
            if(str.charAt(i) == c){
                if(first){
                    sb.append(str.charAt(i));
                    first = false;
                }
            }
            else{
                sb.append(str.charAt(i));
            }
        }
        return sb.toString();
    }
}
