package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium11;

import java.time.LocalDate;

public class Zadanie_4{

	public static void main(String[] args){
		// Musi byc posortowana
		Integer[] a = new Integer[]{3,4,5,6,8};
		int a_search = 4;
		// Musi byc posortowana
		LocalDate[] b = new LocalDate[4];
		b[0] = LocalDate.of(2000,10,12);
		b[1] = LocalDate.of(2000,12,25);
		b[2] = LocalDate.of(2001,4,2);
		b[3] = LocalDate.of(2003,6,19);
		LocalDate b_search = LocalDate.of(2003,6,19);
		System.out.println("a_search: " + ArrayUtil.binSearch(a, a_search));
		System.out.println("b_search: " + ArrayUtil.binSearch(b, b_search));
	}
}
