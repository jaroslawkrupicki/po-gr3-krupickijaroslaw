package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium11;

import java.time.LocalDate;

public class Zadanie_3{

	public static void main(String[] args){
		Integer[] a = new Integer[]{3,4,5,5,6,8};
		Integer[] b = new Integer[]{3,4,5,7,6,8};
		LocalDate[] c = new LocalDate[4];
		c[0] = LocalDate.of(2000,10,12);
		c[1] = LocalDate.of(2000,12,25);
		c[2] = LocalDate.of(2000,12,25);
		c[3] = LocalDate.of(2001,4,2);
		LocalDate[] d = new LocalDate[4];
		d[0] = LocalDate.of(2000,10,12);
		d[1] = LocalDate.of(2000,12,25);
		d[2] = LocalDate.of(2000,11,25);
		d[3] = LocalDate.of(2001,4,2);
		System.out.println("isSorted a: " + ArrayUtil.isSorted(a));
		System.out.println("isSorted b: " + ArrayUtil.isSorted(b));
		System.out.println("isSorted c: " + ArrayUtil.isSorted(c));
		System.out.println("isSorted d: " + ArrayUtil.isSorted(d));
	}
}
