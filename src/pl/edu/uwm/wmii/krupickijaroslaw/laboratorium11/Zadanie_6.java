package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium11;

import java.time.LocalDate;

public class Zadanie_6{

	public static void main(String[] args){
		Integer[] a = new Integer[]{8,7,6,5,3,8,3,0,4,2,8,4,5,1};
		LocalDate[] b = new LocalDate[8];
		b[0] = LocalDate.of(2000,10,12);
		b[1] = LocalDate.of(2000,12,25);
		b[2] = LocalDate.of(2003,9,19);
		b[3] = LocalDate.of(2005,1,28);
		b[4] = LocalDate.of(2002,4,28);
		b[5] = LocalDate.of(2002,4,14);
		b[6] = LocalDate.of(2001,3,30);
		b[7] = LocalDate.of(2004,8,22);
		ArrayUtil.mergeSort(a);
		ArrayUtil.mergeSort(b);
		System.out.print("a: ");
		for(int i : a) System.out.print(i + ",");
		System.out.println();
		System.out.println("b: ");
		for(LocalDate i : b) System.out.println(i.toString());
	}
}
