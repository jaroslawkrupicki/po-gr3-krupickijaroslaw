package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium11;

public class PairUtil<T>{

	public static <T> Pair<T> swap(Pair<T> pair){
		Pair<T> p = new Pair<>();
		p.setFirst(pair.getSecond());
		p.setSecond(pair.getFirst());
		return p;
	}
}
