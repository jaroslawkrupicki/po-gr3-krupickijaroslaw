package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium12;

import java.util.LinkedList;

public class Zadanie_4{

	public static void main(String[] args){
		LinkedList<String> pracownicy = new LinkedList<>();
		pracownicy.add("Nazwisko 1");
		pracownicy.add("Nazwisko 2");
		pracownicy.add("Nazwisko 3");
		pracownicy.add("Nazwisko 4");
		pracownicy.add("Nazwisko 5");
		pracownicy.add("Nazwisko 6");
		pracownicy.add("Nazwisko 7");
		pracownicy.add("Nazwisko 8");
		odwroc(pracownicy);
		System.out.println(pracownicy);
		LinkedList<Integer> liczby = new LinkedList<>();
		liczby.add(1);
		liczby.add(2);
		liczby.add(3);
		liczby.add(4);
		liczby.add(5);
		liczby.add(6);
		liczby.add(7);
		liczby.add(8);
		odwroc(liczby);
		System.out.println(liczby);
	}

	public static <T> void odwroc(LinkedList<T> lista){
		for(int i = 0; i < lista.size() / 2; i++){
			T tmp = lista.get(i);
			lista.set(i, lista.get(lista.size() - 1 - i));
			lista.set(lista.size() - 1 - i, tmp);
		}
	}
}
