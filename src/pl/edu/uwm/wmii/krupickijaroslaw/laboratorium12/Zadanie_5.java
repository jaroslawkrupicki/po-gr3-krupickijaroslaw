package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium12;

import java.util.ArrayList;
import java.util.Stack;

public class Zadanie_5{

	public static void main(String[] args){
		String s = "Ala ma kota. Jej kot lubi myszy.";
		ArrayList<Stack<String>> stacks = new ArrayList<>();
		Stack<String> words_stack = new Stack<>();
		String word = "";
		for(char c : s.toCharArray()){
			if(c == ' '){
				if(word.length() > 0){
					words_stack.add(word);
					word = "";
				}
				continue;
			}
			if(c == '.'){
				if(word.length() > 0){
					words_stack.add(word);
					word = "";
				}
				stacks.add(words_stack);
				words_stack = new Stack<>();
				continue;
			}
			word += String.valueOf(c);
		}
		for(int i = 0; i < stacks.size(); i++){
			boolean first = true;
			while(!stacks.get(i).empty()){
				if(first){
					first = false;
					String firstWord = stacks.get(i).pop();
					firstWord = firstWord.substring(0, 1).toUpperCase() + firstWord.substring(1).toLowerCase();
					System.out.print(firstWord);
				}
				else System.out.print(stacks.get(i).pop().toLowerCase());
				if(stacks.get(i).size() == 0) System.out.print(".");
				else System.out.print(" ");
			}
			if(i < stacks.size() - 1) System.out.print(" ");
		}
	}
}
