package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium12;

import java.util.LinkedList;

public class Zadanie_3{

	public static void main(String[] args){
		LinkedList<String> pracownicy = new LinkedList<>();
		pracownicy.add("Nazwisko 1");
		pracownicy.add("Nazwisko 2");
		pracownicy.add("Nazwisko 3");
		pracownicy.add("Nazwisko 4");
		pracownicy.add("Nazwisko 5");
		pracownicy.add("Nazwisko 6");
		pracownicy.add("Nazwisko 7");
		pracownicy.add("Nazwisko 8");
		odwroc(pracownicy);
		System.out.println(pracownicy);
	}

	public static void odwroc(LinkedList<String> lista){
		for(int i = 0; i < lista.size() / 2; i++){
			String tmp = lista.get(i);
			lista.set(i, lista.get(lista.size() - 1 - i));
			lista.set(lista.size() - 1 - i, tmp);
		}
	}
}
