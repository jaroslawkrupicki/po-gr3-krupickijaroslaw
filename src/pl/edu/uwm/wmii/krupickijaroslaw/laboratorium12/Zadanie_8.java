package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium12;

import java.util.Stack;

public class Zadanie_8{

	public static void main(String[] args){
		Stack<String> stack = new Stack<>();
		stack.add("jeden");
		stack.add("dwa");
		stack.add("trzy");
		String[] list = new String[3];
		list[0] = "jeden";
		list[1] = "dwa";
		list[2] = "trzy";
		print(list);
	}

	public static <E> void print(E[] e){
		for(E i : e){
			System.out.print(i + ",");
		}
	}
}
