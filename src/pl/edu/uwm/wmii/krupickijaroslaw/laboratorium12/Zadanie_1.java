package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium12;

import java.util.LinkedList;

public class Zadanie_1{

	public static void main(String[] args){
		LinkedList<String> pracownicy = new LinkedList<>();
		pracownicy.add("Nazwisko 1");
		pracownicy.add("Nazwisko 2");
		pracownicy.add("Nazwisko 3");
		pracownicy.add("Nazwisko 4");
		pracownicy.add("Nazwisko 5");
		pracownicy.add("Nazwisko 6");
		pracownicy.add("Nazwisko 7");
		pracownicy.add("Nazwisko 8");
		redukuj(pracownicy, 2);
		System.out.println(pracownicy);
	}

	public static void redukuj(LinkedList<String> pracownicy, int n){
		int i = n - 1;
		while(i < pracownicy.size()){
			pracownicy.remove(i);
			i += n - 1;
		}
	}
}
