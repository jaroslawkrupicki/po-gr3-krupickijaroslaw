package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium12;

import java.util.Stack;

public class Zadanie_6{

	public static void main(String[] args){
		int n = 2015;
		Stack<Integer> stack = new Stack<>();
		while(n != 0){
			stack.add(n % 10);
			n /= 10;
		}
		while(!stack.empty()){
			System.out.print(stack.pop() + " ");
		}
	}
}
