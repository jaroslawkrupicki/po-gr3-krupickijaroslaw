package pl.edu.uwm.wmii.krupickijaroslaw.laboratorium12;

import java.util.ArrayList;

public class Zadanie_7{

	public static void main(String[] args){
		int n = 120;
		ArrayList<Integer> zbior = new ArrayList<>();
		for(int i = 2; i <= n; i++) zbior.add(i);
		int wykreslana = 2;
		while(wykreslana <= Math.sqrt(n)){
			for(int i = 0; i < zbior.size(); i++){
				if(wykreslana == zbior.get(i)) continue;
				if(zbior.get(i) % wykreslana == 0) zbior.remove(i);
			}
			wykreslana++;
		}
		System.out.println(zbior);
	}
}
